#include "ARMProcessor.hpp"

#pragma region Assembly Functions

// rotate right
extern "C" uint32_t shifter_ror( uint32_t rotate, uint32_t immediate );

// arithmetic shifts ( they fill vacated bits with the original MSB )
extern "C" uint32_t arithmetic_shl( uint32_t rotate, uint32_t immediate );
extern "C" uint32_t arithmetic_shr( uint32_t rotate, uint32_t immediate );
#pragma endregion

bool ARM::ARMProcessor::ConditionMet( Internals::ARMInstruction * pInstruction )
{
	auto cond = pInstruction->condition;

	/*Think you can only have one condition. not sure.*/

	if ( cond == Internals::Conditions::AL ) // always execute.
		return true;

	if ( cond == Internals::Conditions::CC )
		return ( !this->registers->cpsr.carry );

	if ( cond == Internals::Conditions::CS )
		return ( this->registers->cpsr.carry );

	if ( cond == Internals::Conditions::EQ )
		return ( this->registers->cpsr.zero );

	if ( cond == Internals::Conditions::GE )
		return ( this->registers->cpsr.negative == this->registers->cpsr.overflow );

	if ( cond == Internals::Conditions::GT )
		return ( !this->registers->cpsr.zero && ( this->registers->cpsr.negative == this->registers->cpsr.overflow ) );

	if ( cond == Internals::Conditions::HI )
		return ( this->registers->cpsr.carry && !this->registers->cpsr.zero );

	if ( cond == Internals::Conditions::LE )
		return ( this->registers->cpsr.zero && ( this->registers->cpsr.negative != this->registers->cpsr.overflow ) );

	if ( cond == Internals::Conditions::LS )
		return ( !this->registers->cpsr.carry && this->registers->cpsr.zero );

	if ( cond == Internals::Conditions::LT )
		return ( this->registers->cpsr.negative != this->registers->cpsr.overflow );

	if ( cond == Internals::Conditions::MI )
		return ( this->registers->cpsr.negative );

	if ( cond == Internals::Conditions::NE )
		return ( !this->registers->cpsr.zero );

	if ( cond == Internals::Conditions::PL )
		return ( !this->registers->cpsr.negative );

	if ( cond == Internals::Conditions::VC )
		return ( !this->registers->cpsr.overflow );

	if ( cond == Internals::Conditions::VS )
		return ( this->registers->cpsr.overflow );

	return false; // unpredictable instruction. do not execute.
}

bool ARM::ARMProcessor::UpdateSimpleFlags( Internals::ARMInstruction * pInstruction )
{
	this->registers->cpsr.negative = ( GetBit( REG( pInstruction->rd ), 31 ) == 1 );
	this->registers->cpsr.zero = REG( pInstruction->rd ) == 0;

	return false;
}

std::tuple<ARM::Internals::DataOperandType, uint32_t, uint32_t> ARM::ARMProcessor::DecodeDataProcessing( Internals::ARMInstruction * pInstruction )
{
	using decode = std::tuple<ARM::Internals::DataOperandType, uint32_t, uint32_t>;
	using ops = ARM::Internals::DataOperandType;

	if ( pInstruction->i )
	{
		auto shift = shifter_ror( pInstruction->data.imm.rotate * 2, pInstruction->data.imm.shift );
		decode sc = { ops::imm, shift , pInstruction->data.imm.rotate ? this->GetBit( shift, 31 ) : this->registers->cpsr.carry };
		return sc;
	}
	else
	{
		if ( pInstruction->data.reg.mustbezero == 0 )
		{
			 // register operand
			decode sc = { ops::shlreg, REG( pInstruction->data.shifts.shlreg.rm ), this->registers->cpsr.carry };
			return sc;
		}
		else if ( pInstruction->data.shifts.shlimm.mustbezero == 0 )
		{
			auto operand = REG( pInstruction->data.shifts.shlimm.rm );

			if ( pInstruction->data.shifts.shlimm.shift_amount == 0 )
			{
				decode sc = { ops::shlimm, operand, this->registers->cpsr.carry };
				return sc;
			}
			else
			{
				decode sc = { ops::shlimm,  operand << pInstruction->data.shifts.shlimm.shift_amount, GetBit( operand , 32 - pInstruction->data.shifts.shlimm.shift_amount ) };
				return sc;
			}
		}
		else if ( pInstruction->data.shifts.shlreg.mustbeone == 1 )
		{
			if ( pInstruction->data.shifts.shlreg.mustbezero == 0 )
			{
				auto operand = REG( pInstruction->data.shifts.shlreg.rm );
				auto testval = REG( pInstruction->data.shifts.shlreg.rs ) & 0xFF;
				auto carry = this->registers->cpsr.carry;

				if ( testval < 32 )
					operand <<= testval;
				else if ( testval == 32 )
				{
					carry = GetBit( operand, 0 );
					operand = 0;
				}
				else if ( testval > 32 )
					carry = operand = 0;

				decode sc = { ops::shlreg, operand, carry };
				return sc;
			}
		}
		else if ( pInstruction->data.shifts.shrimm.mustbetwo == 2 )
		{
			uint32_t operand = 0;
			bool carry = this->GetBit( REG( pInstruction->data.shifts.shrimm.rm ), pInstruction->data.shifts.shrimm.shift_amount - 1 );

			if ( pInstruction->data.shifts.shrimm.shift_amount == 0 )
				carry = this->GetBit( REG( pInstruction->data.shifts.shrimm.rm ), 31 );
			else
				operand = REG( pInstruction->data.shifts.shrimm.rm ) >> pInstruction->data.shifts.shrimm.shift_amount;

			decode sc = { ops::shrimm, operand, carry };
			return sc;
		}
		else if ( pInstruction->data.shifts.shrreg.mustbethree == 3 )
		{
			uint32_t operand = REG( pInstruction->data.shifts.shrreg.rm );
			bool carry = this->registers->cpsr.carry;
			auto testval = REG( pInstruction->data.shifts.shrreg.rs ) & 0xFF;

			if ( testval < 32 )
				operand >>= testval;
			else if ( testval == 32 )
			{
				operand = 0;
				carry = this->GetBit( operand, testval - 1 );
			}
			else if ( testval > 32 )
				carry = operand = 0;

			decode sc = { ops::shrreg, operand, carry };
		}
		else if ( pInstruction->data.shifts.asrimm.mustbefour == 4 )
		{
			auto shift = pInstruction->data.shifts.asrimm.shift_amount;
			auto operand = REG( pInstruction->data.shifts.asrimm.rm );
			auto carry = this->GetBit( operand, 31 );

			if ( shift == 0 )
				if ( carry == 0 )
					operand = 0;
				else
					operand = -1;
			else // actually funky how it knows. come back here if theres ever a problem tho lmao.
			{
				carry = this->GetBit( operand, shift - 1 );
				operand = arithmetic_shr( shift, operand );
			}

			decode sc = { ops::asrimm, operand, carry };
			return sc;
		}
		else if ( pInstruction->data.shifts.asrreg.mustbefive == 5 )
		{
			auto shift = REG( pInstruction->data.shifts.asrreg.rs ) & 0xFF;
			auto operand = REG( pInstruction->data.shifts.asrreg.rm );
			auto carry = this->registers->cpsr.carry;

			if ( shift < 32 )
			{
				carry = this->GetBit( operand, shift - 1 );
				operand = arithmetic_shr( shift, operand );
			}
			else if ( shift >= 32 )
			{
				carry = this->GetBit( operand, 31 );
				if ( this->GetBit( operand, 31 ) == 0 )
					operand = 0;
				else
					operand = -1;
			}

			decode sc = { ops::asrimm, operand, carry };
			return sc;
		}
		else if ( pInstruction->data.shifts.rorimm.mustbesix == 6 )
		{
			auto shift = pInstruction->data.shifts.rorimm.shift_amount;
			auto operand = REG( pInstruction->data.shifts.rorimm.rm );
			auto carry = this->GetBit( operand, shift - 1 );

			if ( !shift )
				goto fuckStructure; // actually a different instruction, thanks ARM ARM.
			else
				operand = shifter_ror( shift, operand );

			decode sc = { ops::rorimm, operand, carry };
			return sc;
		}
		else if ( pInstruction->data.shifts.rorreg.mustbeseven == 7 )
		{
			auto shift = REG( pInstruction->data.shifts.rorreg.rs ) & 0x1F;
			auto operand = REG( pInstruction->data.shifts.rorreg.rm );
			auto carry = this->registers->cpsr.carry;

			if ( shift == 0 )
				carry = this->GetBit( operand, 31 );
			else if ( shift > 0 )
			{
				carry = this->GetBit( operand, shift - 1 );
				operand = shifter_ror( shift, operand );
			}

			decode sc = { ops::rorreg, operand, carry };
			return sc;
		}
		else if ( pInstruction->data.shifts.rorext.mustbesix == 6 )
		{
fuckStructure:
			auto operand = ( this->registers->cpsr.carry << 31 ) | ( REG( pInstruction->data.shifts.rorext.rm ) >> 1 );
			auto carry = this->GetBit( REG( pInstruction->data.shifts.rorext.rm ), 0 );

			decode sc = { ops::rorext, operand, carry };
			return sc;
		}
	}

	// invalid
	return { ops::INVALID, 0, 0 };
}

bool ARM::ARMProcessor::GetBit( uint32_t value, int place )
{
	if ( place >= 0 && place < 32 )
		return ( value & ( 1 << place ) >> place );
	else
		return 0;
}

void ARM::ARMProcessor::armADC( Internals::ARMInstruction & instr )
{
	
}

void ARM::ARMProcessor::armADD( Internals::ARMInstruction & instr )
{

}

void ARM::ARMProcessor::armAND( Internals::ARMInstruction & instr )
{

}
